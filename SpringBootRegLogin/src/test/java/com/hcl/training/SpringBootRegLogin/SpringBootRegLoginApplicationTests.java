package com.hcl.training.SpringBootRegLogin;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;

import com.hcl.training.SpringBootRegLogin.model.User;
import com.hcl.training.SpringBootRegLogin.service.UserService;
@ExtendWith(SpringExtension.class)
@WebAppConfiguration
//@WebIntegrationTest
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest
@ContextConfiguration(classes= {TestBeanConfig.class})
public class SpringBootRegLoginApplicationTests {
	@Autowired
	UserService userService;
	User user;
	
	@BeforeEach
	public void setUp() {
		user = new User();
	}
	@Test
	public void testSave() {
		user.setId(1);
		user.setFirstName("sonika");
		user.setLastName("d");
		user.setAge(20);
		user.setGender("f");
		user.setContactNumber("1234567890");
		user.setUserId("A1");
		user.setPassword("sonika");
		user.setRoleId("Admin");
		userService.saveData(user);	
	}
	
}
