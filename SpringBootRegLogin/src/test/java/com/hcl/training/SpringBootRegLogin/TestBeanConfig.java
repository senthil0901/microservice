package com.hcl.training.SpringBootRegLogin;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages= {"com.hcl.training.SpringBootRegLogin"})
public class TestBeanConfig {

}
