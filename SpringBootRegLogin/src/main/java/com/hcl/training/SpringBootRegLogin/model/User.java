package com.hcl.training.SpringBootRegLogin.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.persistence.Table;
import org.hibernate.validator.constraints.Range;
/**
* Topic: Airport Management System
* 
* This class is used to store User details.
* 
* @author sonika-desamsetty
*
*/
@Entity
@Table(name="user")
public class User {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@NotEmpty(message="Please enter id")
	private String firstName;
	@NotEmpty(message="Please enter first name")
	private String lastName;
	@Range(min=18,max=50,message="Please enter last name")
	private int age;
	@NotEmpty(message="Please enter age")
	private String gender;
	@NotEmpty(message="Please enter gender")
	@Pattern(regexp="(^$|[0-9]{10})",message="Enter 10 digit number")
	private String contactNumber;
	@NotEmpty(message="Please enter contact number")
	private String userId;
	@NotEmpty(message="Please enter user id")
	private String password;
	@NotEmpty(message="provide role id")
	private String roleId;
	//private String status;

	public User() {
		super();
	}


	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}


	/**
	 * @return the firstname
	 */
	public String getFirstName() {
		return firstName;
	}


	/**
	 * @param firstname the firstname to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	/**
	 * @return the lastname
	 */
	public String getLastName() {
		return lastName;
	}


	/**
	 * @param lastname the lastname to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}


	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}


	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}


	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}


	/**
	 * @return the phoneNumber
	 */
	public String getContactNumber() {
		return contactNumber;
	}


	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}


	/**
	 * @return the vendorId
	 */
	public String getUserId() {
		return userId;
	}



	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}


	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}


	/**
	 * @return the roleId
	 */
	public String getRoleId() {
		return roleId;
	}


	/**
	 * @param roleId the roleId to set
	 */
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	


	

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}


	/**
	 * @param id
	 * @param firstname
	 * @param lastname
	 * @param age
	 * @param gender
	 * @param phoneNumber
	 * @param userId
	 * @param password
	 */
	public User(int id, @NotEmpty String firstName, @NotEmpty String lastName,
			@Range(min = 18, max = 50) int age, @NotEmpty String gender, @NotEmpty String contactNumber,
			@NotEmpty String vendorId, @NotEmpty String password,String roleId) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.gender = gender;
		this.contactNumber = contactNumber;
		this.userId = vendorId;
		this.password = password;
		this.roleId = roleId;
		//this.status = status;
	}


	

	
	

}
