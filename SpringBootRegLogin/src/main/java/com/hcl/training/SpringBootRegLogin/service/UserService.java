package com.hcl.training.SpringBootRegLogin.service;

import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.stereotype.Service;

import com.hcl.training.SpringBootRegLogin.model.User;

@Transactional

public interface UserService {
	public void saveData(User user);

	public List<User> getAllUsers();

	public boolean login(@Valid User user); 
}
