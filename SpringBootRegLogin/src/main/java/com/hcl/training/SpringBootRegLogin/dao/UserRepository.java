package com.hcl.training.SpringBootRegLogin.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.hcl.training.SpringBootRegLogin.model.User;
@Repository
@Component 
public interface UserRepository extends CrudRepository<User,Integer>{
	

}
