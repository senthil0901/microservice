package com.hcl.training.SpringBootRegLogin.service;

import java.util.Base64;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

//import com.hcl.training.SpringBootRegLogin.dao.UserDao;
import com.hcl.training.SpringBootRegLogin.dao.UserRepository;
import com.hcl.training.SpringBootRegLogin.model.User;
@Service
@Transactional
@Component
public class UserServiceImpl implements UserService{
	//@Autowired
	//UserDao userDao;
	@Autowired
	UserRepository repo;
	public void saveData(User user) {
		String encryptedPassword= Base64.getEncoder().encodeToString(user.getPassword().getBytes());
		user.setPassword(encryptedPassword);
		repo.save(user);
		//userDao.saveData(user);
	}
	@Override
	public List<User> getAllUsers() {
		
		return (List<User>) repo.findAll();
	}
	@Override
	public boolean login(@Valid User user) {
		String encryptedPassword= Base64.getEncoder().encodeToString(user.getPassword().getBytes());
		user.setPassword(encryptedPassword);
					Optional<User> list = repo.findById(user.getId());
			if(list.get().getPassword().contentEquals(encryptedPassword)) {
				return true;
			}
			else {
				return false;
			}
		
		
		
	}

}
