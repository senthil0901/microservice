package com.hcl.training.SpringBootRegLogin.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hcl.training.SpringBootRegLogin.model.User;
import com.hcl.training.SpringBootRegLogin.service.UserService;
import com.hcl.training.SpringBootRegLogin.service.UserServiceImpl;
@Controller
@Component
public class HomeController {
	@Autowired
	UserService userService;
	@PostMapping(value = { "/save"})
	public ModelAndView register1(@Valid @ModelAttribute("user") User user,BindingResult result) {
		if(result.hasErrors()) {
			return new ModelAndView("register");
		}
		else {
		userService.saveData(user);
		return new ModelAndView("success");
		}
	}
	 
	
	@GetMapping(value = { "/register"})   
	public ModelAndView register(@ModelAttribute("user") User user) {
		return new ModelAndView("register");
	}
	@GetMapping(value="/fetchAll")
	public ModelAndView fetchData(@ModelAttribute("user") User user) {
		List<User> usr = userService.getAllUsers();
		return new ModelAndView("listUsers","listUsers",usr);
	}
	@GetMapping(value = { "/login"})   
	public ModelAndView login(@ModelAttribute("user") User user) {
		return new ModelAndView("login");
	}
	@PostMapping(value = { "/login"})
	public ModelAndView login(@Valid @ModelAttribute("user") User user,BindingResult result) {
		if(result.hasErrors()) {
			return new ModelAndView("register");
		}
		else {
		if(userService.login(user)) {
			return new ModelAndView("success");
		}
		return new ModelAndView("failed");
		}
	}
	
}
