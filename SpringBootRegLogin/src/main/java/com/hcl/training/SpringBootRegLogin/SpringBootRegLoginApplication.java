package com.hcl.training.SpringBootRegLogin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
@EnableJpaRepositories("com.hcl.training.SpringBootRegLogin")
@ComponentScan(basePackages = {"com.hcl.training.SpringBootRegLogin"})
@SpringBootApplication
public class SpringBootRegLoginApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRegLoginApplication.class, args);
	}

}
