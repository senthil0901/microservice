<%@ page session="true" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>

<html>
<head>
<meta charset="ISO-8859-1">
<style>
body {
	background-image: url('/bg.png');
}
</style>
<title>Registration</title>
</head>
<body>

	<div align="center">
		<h1>Welcome to Registration Page</h1>
		<form:form action="save" method="post" modelAttribute="user">
			<table border=1>
				<tr>
					<td>Firstname</td>
					<td><form:input type="text" path="firstName" placeholder="Enter your first name"
						style="width: 120px"/><font color="red"><form:errors path="firstName"/></font></td>
				</tr>
				<tr>
					<td>Lastname</td>
					<td><form:input type="text" path="lastName" placeholder="Enter your last name"
						style="width: 120px" /><font color="red"><form:errors path="lastName"/></font></td>
				</tr>
				<tr>
					<td>age</td>
					<td><form:input type="text" path="age" placeholder="Enter your age"
						style="width: 120px"/><font color="red"><form:errors path="age"/></font></td>
				</tr>
				<tr>
					<td>gender</td>
					<td><form:radiobutton path="gender" value="M"/>Male 
<form:radiobutton path="gender" value="F"/>Female <font color="red"><form:errors path="gender"/></font>
</td>
				</tr>
				<tr>
					<td>phone number</td>
					<td><form:input type="text" path="contactNumber" placeholder="Enter your phone number"
						style="width: 120px"/><font color="red"><form:errors path="contactNumber"/></font></td>
				</tr>
				<tr>
					<td>User id</td>
					<td><form:input type="text" path="userId" placeholder="Enter your user id"
						style="width: 120px"/><font color="red"><form:errors path="userId"/></font></td>
				</tr>
				<tr>
					<td>Password</td>
					<td><form:input type="password" path="password" placeholder="Enter your password"
						style="width: 120px"/><font color="red"><form:errors path="password"/></font></td>
				</tr>
				<tr>
					<td>RoleId</td>
					<td><form:select path="roleId" id="roleId" style="width: 127px">
					<option value="">select</option>
							<option value="ROLE_ADMIN">Admin</option>
							<option value="ROLE_USER">User</option>
					</form:select><font color="red"><form:errors path="roleId"/></td>
				</tr>
				
			</table>
			<input type="submit" value="Register">
		</form:form><br></br>
		Already Registered?<a href="userLogin">Login</a></br>
		Click here to go to Homepage <a href="homepage">Home</a>
	</div>
</body>
</html>