<%@ page session="true" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>

<html>
<head>
<meta charset="ISO-8859-1">
<style>
body {
	background-image: url('/bg.png');
}
</style>
<title>Login</title>
</head>
<body>

	<div align="center">
		<h1>Welcome to Login Page</h1>
		<form:form action="login" method="post" modelAttribute="user">
		<form:input type="hidden" path="firstName" value="a"/>
		<form:input type="hidden" path="lastName" value="a"/>
		<form:input type="hidden" path="age" value="20"/>
		<form:input type="hidden" path="gender" value="a"/>
		<form:input type="hidden" path="userId" value="a"/>
		<form:input type="hidden" path="contactNumber" value="1234567890"/>
		<form:input type="hidden" path="roleId" value="a"/>
			<table border=1>
				<tr>
					<td>Id</td>
					<td><form:input type="text" path="id" placeholder="Enter your id"
						style="width: 120px"/><font color="red"><form:errors path="id"/></font></td>
				</tr>
				
				<tr>
					<td>Password</td>
					<td><form:input type="password" path="password" placeholder="Enter your password"
						style="width: 120px"/><font color="red"><form:errors path="password"/></font></td>
				</tr>
				
			</table>
			<input type="submit" value="Login">
		</form:form><br></br>
		
	</div>
</body>
</html>