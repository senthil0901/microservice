package com.pack.DoctorServiceSleuth.api;

public interface DoctorsServiceApi {
    String getDoctors(String hospital);
}