package com.pack.DoctorServiceSleuth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DoctorServiceSleuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(DoctorServiceSleuthApplication.class, args);
	}

}
