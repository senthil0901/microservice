package com.pack.DiseasesServiceSleuth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DiseasesServiceSleuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiseasesServiceSleuthApplication.class, args);
	}

}
