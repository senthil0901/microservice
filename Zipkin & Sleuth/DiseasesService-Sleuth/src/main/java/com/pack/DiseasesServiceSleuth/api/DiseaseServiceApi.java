package com.pack.DiseasesServiceSleuth.api;

public interface DiseaseServiceApi {
    String getDiseases(String germ);
}