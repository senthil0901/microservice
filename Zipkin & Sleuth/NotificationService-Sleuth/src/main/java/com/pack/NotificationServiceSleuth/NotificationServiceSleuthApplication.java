package com.pack.NotificationServiceSleuth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NotificationServiceSleuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(NotificationServiceSleuthApplication.class, args);
	}

}
