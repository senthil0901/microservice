package com.pack.PatientsServiceSleuth.api;

public interface PatientServiceApi {
    String getPatients(String city);
}