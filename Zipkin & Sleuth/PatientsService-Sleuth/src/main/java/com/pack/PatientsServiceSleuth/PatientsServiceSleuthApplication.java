package com.pack.PatientsServiceSleuth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PatientsServiceSleuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(PatientsServiceSleuthApplication.class, args);
	}

}
