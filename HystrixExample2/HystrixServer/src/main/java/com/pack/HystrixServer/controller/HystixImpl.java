package com.pack.HystrixServer.controller;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Component
public class HystixImpl {

	
	@Autowired
	private RestTemplate restTemplate;
	
	
	@HystrixCommand(fallbackMethod = "defaultFallbackCallGetName")
	public ResponseEntity<String> getName(){
		try{
			System.out.println("*********************Inside Main*******************");
			URI endPoint = URI.create("http://localhost:8083/getName");
			return restTemplate.getForEntity(endPoint, String.class);
		}catch(Exception e){
			throw new RuntimeException();
		}
		
	}
	
	public ResponseEntity<String> defaultFallbackCallGetName(){
		System.out.println("*********************Inside fallback*******************");
		URI endPoint=URI.create("http://localhost:8084/getName");
		return restTemplate.getForEntity(endPoint, String.class);
	}
	
	
}

