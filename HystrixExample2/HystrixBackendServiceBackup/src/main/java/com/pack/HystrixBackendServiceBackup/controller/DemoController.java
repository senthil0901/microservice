package com.pack.HystrixBackendServiceBackup.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

	@RequestMapping("/getName")
	public String getDetails() {
		return "Value return from Backend backup service!!!";
	}
}
