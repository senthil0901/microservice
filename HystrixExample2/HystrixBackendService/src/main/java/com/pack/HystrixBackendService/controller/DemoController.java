package com.pack.HystrixBackendService.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

	@RequestMapping("/getName")
	public String getDetails() {
		return "Value return from Backend service!!!";
	}
}
