package com.pack.RibbonMicroservice1;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

    @Value("${server.port}")
    private String port;

    private int counter;

    @GetMapping("/allServices")
    public String service() {
        counter ++;
        return counter + " ==> List of Services new [ response from " + port + "]";
    }
}
