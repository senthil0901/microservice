package com.pack.RibbonMicroservice1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RibbonMicroservice1Application {

	public static void main(String[] args) {
		SpringApplication.run(RibbonMicroservice1Application.class, args);
	}

}
