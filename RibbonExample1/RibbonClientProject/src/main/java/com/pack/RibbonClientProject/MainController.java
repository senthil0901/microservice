package com.pack.RibbonClientProject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class MainController {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/services")
    public String getDoctors(){
        String url = "http://ribbon-service/allServices";//http://localhost:9081/allServices";
        return restTemplate.getForObject(url, String.class);
    }
}