package com.pack.DepartmentService.service;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pack.DepartmentService.entity.Department;
import com.pack.DepartmentService.repository.DepartmentRepository;

@Service
public class DepartmentService {

	 private Logger log=Logger.getLogger(DepartmentService.class);
	    
    @Autowired
    private DepartmentRepository departmentRepository;

    public Department saveDepartment(Department department) {
        log.info("Inside saveDepartment of DepartmentService");
        return departmentRepository.save(department);
    }

    public Department findDepartmentById(Long departmentId) {
        log.info("Inside saveDepartment of DepartmentService");
        return departmentRepository.findByDepartmentId(departmentId);
    }
}