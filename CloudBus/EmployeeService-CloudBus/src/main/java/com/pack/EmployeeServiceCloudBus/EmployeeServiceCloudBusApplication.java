package com.pack.EmployeeServiceCloudBus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeServiceCloudBusApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeServiceCloudBusApplication.class, args);
	}

}
