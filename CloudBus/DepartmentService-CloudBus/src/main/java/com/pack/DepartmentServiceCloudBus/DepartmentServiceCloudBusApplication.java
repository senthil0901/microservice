package com.pack.DepartmentServiceCloudBus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DepartmentServiceCloudBusApplication {

	public static void main(String[] args) {
		SpringApplication.run(DepartmentServiceCloudBusApplication.class, args);
	}

}
