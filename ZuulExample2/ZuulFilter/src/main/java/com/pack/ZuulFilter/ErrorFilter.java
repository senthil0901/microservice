package com.pack.ZuulFilter;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

public class ErrorFilter extends ZuulFilter {

	@Override
	public Object run() throws ZuulException {
		RequestContext context=RequestContext.getCurrentContext();
		HttpServletRequest req=context.getRequest();
		if(req.getMethod().equalsIgnoreCase("post")) {
			try {
				System.out.println("--------Error filter executed-------- "+req.getRequestURI()+"---"+IOUtils.toString(req.getReader()));	
			}
			catch(IOException e) {
				e.printStackTrace();
			}
		}
		if(req.getMethod().equalsIgnoreCase("get")) {
			System.out.println("--------Error filter executed-------- "+req.getRequestURI());
		}
		return null;
	}

	@Override
	public boolean shouldFilter() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public int filterOrder() {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public String filterType() {
		// TODO Auto-generated method stub
		return "error";
	}

}
