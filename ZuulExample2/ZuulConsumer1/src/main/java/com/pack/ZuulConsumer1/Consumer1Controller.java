package com.pack.ZuulConsumer1;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/consumer1")
public class Consumer1Controller {
	
	@RequestMapping(value="/get/{name}",method=RequestMethod.GET)
	public String get(@PathVariable("name")String name) {
		System.out.println("Consumer1 response "+name);
		return "Consumer1 response "+name;
	} 
	
	@RequestMapping(value="/post",method=RequestMethod.POST)
	public String post(@RequestBody User user) {
		System.out.println("Consumer1 post response "+user.getName());
		return "Consumer1 post response "+user.getName();
	}

}
