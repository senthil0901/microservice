package com.pack.ZuulConsumer1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class ZuulConsumer1Application {

	public static void main(String[] args) {
		SpringApplication.run(ZuulConsumer1Application.class, args);
	}

}
