package com.pack.SpringConfigClient2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringConfigClient2Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringConfigClient2Application.class, args);
	}

}
