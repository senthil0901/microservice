package com.pack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class SpringConfigServer1Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringConfigServer1Application.class, args);
	}

}
