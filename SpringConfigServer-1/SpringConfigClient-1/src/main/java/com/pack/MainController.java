package com.pack;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

//contains REST endpoints 
@RestController
@RefreshScope
public class MainController {

	//takes value of channel.source from properties file
    @Value("${channel.source: No Channel}")
    private String source;

    //url and name will come from configprops using channel prefix 
    @Autowired
    private ConfigProps configProps;

    @GetMapping("/info")
    public ChannelInfo getChannleInfo(){
        return new ChannelInfo(source, configProps.getName(), configProps.getUrl());
    }
}