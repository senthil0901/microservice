package com.pack;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

//configuration properties from which we can get values from configuration
//channel.name, channel.url will populated in ConfigProps and injected in controller

@Configuration
@ConfigurationProperties(prefix = "channel")
public class ConfigProps {

    private String name;

    private String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}