package com.pack.PaymentServiceRibbon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class PaymentServiceRibbonApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaymentServiceRibbonApplication.class, args);
	}

}
