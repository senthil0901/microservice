package com.pack.ShoppingServerRibbon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class ShoppingServerRibbonApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShoppingServerRibbonApplication.class, args);
	}

}
