package com.pack.FeignRestApp.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

	@GetMapping("/version")
	public String getVersion() {
		return "(\"Version\" : 1.0)";
	}
}
