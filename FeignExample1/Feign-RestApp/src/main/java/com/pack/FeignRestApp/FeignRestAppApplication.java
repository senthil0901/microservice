package com.pack.FeignRestApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class FeignRestAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(FeignRestAppApplication.class, args);
	}

}
