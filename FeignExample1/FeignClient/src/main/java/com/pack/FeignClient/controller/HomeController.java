package com.pack.FeignClient.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pack.FeignClient.clients.RestClient;

@RestController
public class HomeController {
	
	private RestClient restClient;
	
	@Autowired
	public HomeController(RestClient restClient) {
		this.restClient=restClient;
	}
	
	@GetMapping("/rest-version")
	public String getVersion() {
		return restClient.getVersion();
	}

}
