package com.pack.EmployeeEurekaServerRibbon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class EmployeeEurekaServerRibbonApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeEurekaServerRibbonApplication.class, args);
	}

}
